//set up dotenv 
require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

//local imports
const routes = require('./routes/index')

const app = express()
const router = express.Router()

const environment = process.env.NODE_ENV //development
const stage = require('./config')[environment]

//Connect with mongo_db
mongoose.connect(process.env.MONGO_DB_URI,
    { 
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true 
    }
    , (err)=>{
    console.log("Connected to DB successfully")
})

//Enable CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
  });

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use('/uploads', express.static('uploads'))

if(environment !== 'production'){
    const logger = require('morgan')
    app.use(logger('dev'))
}

//Set up routes middleware
app.use('/api/v1', routes(router));

app.listen(`${stage.port}`, ()=>{
    console.log(`Server now listening at localhost:${stage.port}`)
})

module.exports = app