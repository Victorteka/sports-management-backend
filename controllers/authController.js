const User = require('../models/User')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const {registerValidation, loginValidation} = require('../utils/validation')

module.exports = {
    register: async(req, res) =>{
        //validate data
        const  {error} = registerValidation(req.body) 
        if (error) return res.status(400).json({message:error.details[0].message})

        //Check if email exist
        const emailExist = await User.findOne({email: req.body.email})
        if (emailExist) return res.status(400).json({message:'Email already exist!'})

        //Hash password
        const salt = await bcrypt.genSalt(10)
        const hashedPassword =await bcrypt.hash(req.body.password, salt)

        const user = new User({
            name: req.body.name,
            email: req.body.email,
            password: hashedPassword
        })

        try{
            const savedUser = await user.save()
            res.json(savedUser)
        }catch(err){
            res.json(err).status(400)
        }

    }, 
    login: async (req, res) =>{
        //Validate data
        const {error} = loginValidation(req.body)
        if(error) return res.json({message:error.details[0].message})

        //Check if email exists
        const user = await User.findOne({email:req.body.email})
        if(!user) return res.status(400).json({message:"Email not found!"})

        //check if password is valid
        const validPassword = await bcrypt.compare(req.body.password, user.password)
        if(!validPassword) return res.status(400).json({message:"Invalid password"})

        //Create token
        const token = jwt.sign({user: user}, process.env.JWT_SECRET)
        
        res.json({message: "Logged in successfully", token: token})
    }
}

