const League = require('../models/League')
const {leagueValidation} = require('../utils/validation')


module.exports = {
    addLeague: async (req, res) =>{
        const {error} = leagueValidation(req.body)
        if(error) return res.json({message:error.details[0].message})

        const leagueExist = await League.findOne({name: req.body.name})
        if(leagueExist) return res.json({message: "League already exists!"}).status(400)

        const league = new League({
            name: req.body.name,
            teams: req.body.teams
        })

        try{
            savedLeague = await league.save()
            res.json({message:"League created successful!", league: savedLeague})
        }catch(err){
            res.json({message:err}).status(400)
        }
    },

    getLeagues: async (req, res) => {
        try{
            const leagues = await League.find()
            return res.json({leagues: leagues})
        }catch(err){
            return res.json({message: err})
        }
    }
}