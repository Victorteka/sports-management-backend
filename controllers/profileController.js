const User = require('../models/User')
const {profileValidation} = require('../utils/validation')

module.exports = {
    getProfile: async (req, res) =>{
        const savedUser = await User.findById(req.params._id)
        return res.json({user: savedUser})
    },

    updateProfile: async (req, res)=> {
        const {error} = profileValidation(req.body)
        // if(error) return res.json({message:error.details[0].message})
        
        if(!req.file){
            console.log('No file received!')
            return res.send({
                success: false
              })
        }else{
            const host = req.hostname 
            const filePath = req.protocol + '://' + host + ':8000' + '/'+ req.file.path 
            updatedUser = await User.updateOne({
                _id: req.params._id
            },
            {
                $set: {'avater':filePath}
            })     
            const newUser = await User.findById(req.params._id)       
            return res.json({
                message: 'User updated successfully!',
                user: newUser
            })
        }
    }
}