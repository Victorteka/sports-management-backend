const Sport = require('../models/Sport')

const {sportValidation} = require('../utils/validation')

module.exports = {
    addSport: async (req, res)=>{
        //Validate user data
        const {error} = sportValidation(req.body)
        if(error) return res.json({message:error.details[0].message}).status(400)

        //Check if sport exists
        const sportExist = await Sport.findOne({name:req.body.name})
        if(sportExist) return res.status(400).json({message:"Sport already created!"})

        //create sport
        const filepath = req.protocol + '://' + req.host + ':8000' + '/'+ req.file.path
        const sport = new Sport({
            name: req.body.name,
            players: req.body.players,
            coach: req.body.coach,
            captain: req.body.captain,
            trainingDays: req.body.trainingDays,
            thumbnail: filepath
        })

        try{
            const savedSport = await sport.save()
            res.json({message:"Sport created!", sport:savedSport})
        }catch(err){
            res.json({message:err}).status(400)
        }
    },
    getSports: async(req, res) =>{
        try{
            const sports = await Sport.find()
            res.json({sports: sports})
        }catch(err){
            res.json({message:err})
        }
    },

    getSingleSport: async(req, res)=>{
        try{
            const sport = await Sport.findById(req.params.sport_id)
            res.json(sport)
        }catch(err){
            res.json({message:err})
        }
    },

    joinSport: async (req, res) =>{
        try{
            res.send('Joined successfully')
        }catch(err){
            res.json({message:err})
        }
    }
}