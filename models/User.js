const mongoose = require('mongoose')


const userSchema = mongoose.Schema({
    name:{
        type: String,
        required: true,
        min: 6,
        max: 255,
        trim: true
    },
    email:{
        type: String,
        required: true,
        max: 255,
        min: 6,
        trim: true
    },
    password:{
        type: String,
        required: true,
        max: 1024,
        min: 6,
        trim: true
    },
    isAdmin:{
        type: Boolean,
        default: false
    },
    date:{
        type: Date,
        default: Date.now
    },
    avater:{
        type: String,
        trim: true,
        default: null
    },
    sport:{
        type: String,
        trim: true,
        default: null    
    }
})

module.exports = mongoose.model('User', userSchema)


