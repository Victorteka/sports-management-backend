const mongoose = require('mongoose')


const SportSchema = mongoose.Schema({
    name:{
        type: String,
        required: true,
        max: 255,
        trim: true
    },
    players:{
        type: Array,
        require: true,
    },
    coach:{
        type: String,
        required: true,
        min: 6,
        max: 255,
        trim: true    
    },
    captain:{
        type: String,
        required: true,
        min: 6,
        max: 255,
        trim: true    
    },
    trainingDays:{
        type: Array,
        required: true
    },
    thumbnail:{
        type: String,
        required: true
    }    
})

module.exports = mongoose.model('Sport', SportSchema)