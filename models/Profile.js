const mongoose = require('mongoose')


const ProfileSchema = mongoose.Schema({
    name:{
        type: String,
        required: true,
        min: 6,
        max: 255,
        trim: true
    },
    email:{
        type: String,
        required: true,
        max: 255,
        min: 6,
        trim: true
    },
    avater:{
        type: String,
        trim: true
    },
    join:{
        type: Date
    },
    sport:{
        type: String,
        trim: true    
    }
})

module.exports = mongoose.model('Profile', ProfileSchema)