const mongoose = require('mongoose')


const LeagueSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        max: 255,
        trim: true
    },
    teams:{
        type: Array,
        require: true
    },
    startDate: {
        type: Date,
        default: Date.now
    },
    endDate:{
        type: Date
    }
})

module.exports = mongoose.model('League', LeagueSchema)