
module.exports = {
    development:{
        port: process.env.PORT || 8000
    },
    production:{
        port: process.env.PORT || 5000
    }
}