const multer = require('multer')

const profileController = require('../controllers/profileController')
const {validateToken} = require('../utils/validateToken')
const storage = require('../utils/createMulter')

const fileFilter = (req, file, callback)=>{
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
        console.log('rfhfbhrfbh')
        callback(null, true)
    }else{
        callback(null, false)
    }
}

const upload = multer({
    storage: storage,
    limits:{
    fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
})

module.exports = (router) =>{
    router.route('/profile/:_id')
        .get(validateToken, profileController.getProfile)
        .put(validateToken, upload.single('file'), profileController.updateProfile)
}