const controller = require('../controllers/authController')


module.exports = (router) =>{
    router.route('/auth/register')
        .post(controller.register)
}