const loginController = require('../controllers/authController')


module.exports = (router) =>{
    router.route('/auth/login')
        .post(loginController.login)
}