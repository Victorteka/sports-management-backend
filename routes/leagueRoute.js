const leagueController = require('../controllers/leagueController')
const {validateIsAdmin} = require('../utils/validateToken')


module.exports = (router) =>{
    router.route('/leagues')
        .post(validateIsAdmin, leagueController.addLeague)
        .get(leagueController.getLeagues)
}