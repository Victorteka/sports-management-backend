const singleSportController = require('../controllers/sportsController')


module.exports = (router) =>{
    router.route('/sports/:sport_id')
        .post(singleSportController.joinSport)
        .get(singleSportController.getSingleSport)
}