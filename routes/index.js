const authRegister = require('./register')
const authLogin = require('./login')
const sportsRoute = require('./sportsRoutes')
const singleSportRoute = require('./singleSportRoute')
const leagueRoute = require('./leagueRoute')
const profileRoute = require('./profileRoute')


module.exports = (router) =>{
    authRegister(router)
    authLogin(router)
    sportsRoute(router)
    singleSportRoute(router)
    leagueRoute(router)
    profileRoute(router)
    return router
}