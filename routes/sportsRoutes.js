const sportsController = require('../controllers/sportsController')
const {validateIsAdmin} = require('../utils/validateToken')


module.exports = (router)=>{
    router.route('/sports')
        .post(validateIsAdmin, sportsController.addSport)
        .get(sportsController.getSports)
}