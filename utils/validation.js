const Joi = require('@hapi/joi')

//Register validation
const registerValidation = (data) =>{
    const schema = Joi.object({
        name: Joi.string()
        .min(6)
        .required(),

        email: Joi.string()
                .min(6)
                .required()
                .email(),
        password: Joi.string()
                .min(6)
                .required(),
        isAdmin: Joi.boolean()

    })
    return schema.validate(data)
}

//Login validation
const loginValidation = (data) =>{
    const schema = Joi.object({
        email: Joi.string()
                .min(6)
                .required()
                .email(),
        password: Joi.string()
                .min(6)
                .required()
    })
    return schema.validate(data)
}

//Sport validation
const sportValidation = (data) =>{
    const schema = Joi.object({
        name: Joi.string()
            .required(),
        players: Joi.array()
            .required(),

        coach: Joi.string()
            .min(6)
            .required(),

        captain: Joi.string()
            .min(6)
            .required(),
        trainingDays: Joi.array()
            .required(),
        thumbnail: Joi.string()
            .required()
    })
    return schema.validate(data)
}

//Profile validation
const profileValidation = (data) =>{
    const schema = Joi.object({
        sport: Joi.string()
            .required()
    })
    return schema.validate(data)
}

//League validation
const leagueValidation = (data) => {
    const schema = Joi.object({
        name: Joi.string()
            .required(),
        teams: Joi.array()
            .required()
        
    })
    return schema.validate(data)
}

module.exports.registerValidation = registerValidation
module.exports.loginValidation = loginValidation
module.exports.sportValidation = sportValidation
module.exports.leagueValidation = leagueValidation
module.exports.profileValidation = profileValidation