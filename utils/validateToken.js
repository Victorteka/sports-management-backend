const jwt = require('jsonwebtoken')


const validateToken = (req, res, next) =>{
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if(token == null) return res.status(401).json({
        message:'You need to login first to access this endpoint!'
    })
    
    jwt.verify(token, process.env.JWT_SECRET, (err, user)=>{
        if(err){
            console.log(err)
            return res.status(403).json({
                message: "You are forbidden from accessing this endpoint!"
            })
        }
        req.user = user
        next()
    }) 
}

const validateIsAdmin = (req, res, next) =>{
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if(token == null) return res.status(401).json({
        message:'You need to login first to access this endpoint!'
    })
    
    jwt.verify(token, process.env.JWT_SECRET, (err, user)=>{
        if(err){
            console.log(err)
            return res.status(403).json({
                message: "Invalid token!"
            })
        }
        req.user = user
        if(!user.user.isAdmin)return res.status(401).json({
            message:'Admin privilege required!'
        })
        next()
    })
    
}

module.exports.validateToken = validateToken
module.exports.validateIsAdmin = validateIsAdmin